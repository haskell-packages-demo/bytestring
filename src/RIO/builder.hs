{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import RIO
    ( ($), (<>)
    , IO
    , logInfo
    , runSimpleApp
    , Utf8Builder
    )

main :: IO ()
main = runSimpleApp $ do
    logInfo ("Hello " <> "there " <> "world" :: Utf8Builder)
