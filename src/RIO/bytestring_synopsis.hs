{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import           RIO
    ( ($), (/=), (<>), (==)
    , display
    , displayShow
    , IO
    , logInfo
    -- , max
    , runSimpleApp
    , Word8
    )
import qualified RIO.ByteString as S
    ( filter
    -- , foldl1'
    , length
    , readFile
    , take
    , takeWhile
    , writeFile
    )


main :: IO ()
main = runSimpleApp $ do
    S.writeFile "content.txt" "This is some sample content"
    bs <- S.readFile "content.txt"
    logInfo $ displayShow bs
    logInfo $ displayShow $ S.takeWhile (/= space) bs
    logInfo $ displayShow $ S.take 5 bs
    logInfo $ "File contents: " <> displayShow bs

    -- putStrLn $ "Largest byte: " ++ show (S.foldl1' max bs)
    -- Or just use S.maximum

    logInfo $ "Spaces: " <> display (S.length $ S.filter (== space) bs)
  where
    space :: Word8
    space = 32 -- ASCII code
