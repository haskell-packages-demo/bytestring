{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import Prelude (IO, print)

import Data.Monoid ((<>))
import Data.ByteString.Builder
    ( Builder
    , toLazyByteString
    )

main :: IO ()
main = print (toLazyByteString ("Hello " <> "there " <> "world" :: Builder))
